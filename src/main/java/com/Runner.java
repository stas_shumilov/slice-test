package com;

import com.api.WebApi;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Optional;
import com.internal.FileLoader;
import com.internal.WordsManager;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;

import java.io.File;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by sshumilov on 17.06.16.
 */
public class Runner extends Application<Runner.AppConfiguration>{

    @Override
    public void run(final AppConfiguration appConfiguration,
                    final Environment environment) throws Exception {
        final String[] filePaths = Optional.fromNullable(appConfiguration.getFiles()).or(new String[0]);
        final String[] folderPaths =  Optional.fromNullable(appConfiguration.getFolders()).or(new String[0]);

        final Stream<File> filesFromFolders = Arrays.stream(folderPaths)
                .map(File::new)
                .filter(File::isDirectory)
                .flatMap(folder -> Arrays.stream(folder.listFiles()));
        final Stream<File> filesStream = Arrays.stream(filePaths).map(File::new);

        final File[] filesToLoad = Stream.concat(filesFromFolders, filesStream).filter(File::isFile).toArray(File[]::new);
        if (filesToLoad.length > 0) {
            final FileLoader.CounterMaps maps = new FileLoader().load(filesToLoad);
            final WebApi resource = new WebApi(new WordsManager(maps.getWordCounterMap(), maps.getWordAccessMap()));
            environment.jersey().register(resource);
        } else {
            throw new IllegalArgumentException("No files to load. Please check config.yaml");
        }
    }

    static class AppConfiguration extends Configuration {

        private String[] files;
        private String[] folders;

        public String[] getFolders() {
            return folders;
        }

        public void setFolders(final String[] folders) {
            this.folders = folders;
        }

        @JsonProperty
        public String[] getFiles() {
            return files;
        }

        public void setFiles(final String[] files) {
            this.files = files;
        }
    }
}
