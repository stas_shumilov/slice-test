package com.domain;

/**
 * Created by sshumilov on 17.06.16.
 */
public class WordInfo {

    private final String word;
    private final long callCount;
    private final long occurringCount;

    public WordInfo(final String word, final long callCount, final long occurringCount) {
        this.word = word;
        this.callCount = callCount;
        this.occurringCount = occurringCount;
    }

    public String getWord() {
        return word;
    }

    public long getCallCount() {
        return callCount;
    }

    public long getOccurringCount() {
        return occurringCount;
    }
}
