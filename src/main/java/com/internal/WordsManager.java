package com.internal;

import com.domain.WordInfo;
import com.google.common.collect.ImmutableMap;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;

/**
 * Created by sshumilov on 17.06.16.
 */
public class WordsManager {

    private final ImmutableMap<String, Long> wordCounterMap;
    private final ConcurrentHashMap<String, LongAdder> wordAccessMap;

    public WordsManager(final ImmutableMap<String, Long> wordCounterMap,
                        final ConcurrentHashMap<String, LongAdder> wordAccessMap) {
        this.wordCounterMap = wordCounterMap;
        this.wordAccessMap = wordAccessMap;
    }

    public WordInfo findWordInfo(String word) {
        final Long countInFiles = wordCounterMap.getOrDefault(word, 0L);
        LongAdder callCounter = wordAccessMap.get(word);
        if (callCounter == null) {
            callCounter = new LongAdder();
            final LongAdder prevAdder = wordAccessMap.putIfAbsent(word, callCounter);
            if (prevAdder != null) {
                callCounter = prevAdder;
            }
        }
        callCounter.increment();
        return new WordInfo(word, callCounter.sum(), countInFiles);
    }


    public long getUniqueWordsCount() {
        return wordCounterMap.keySet().size();
    }

}
