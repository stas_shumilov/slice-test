package com.internal;

import com.domain
        .WordInfo;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

/**
 * Created by sshumilov on 17.06.16.
 */
public class FileLoader {

    private static final Logger log = LoggerFactory.getLogger(FileLoader.class);

    public CounterMaps load(File[] files) throws IOException {
        Map<String, Long> wordCounterMap = new HashMap<>(100000);
        final ConcurrentHashMap<String, LongAdder> accessCounterMapBuilder = new ConcurrentHashMap<>(100000);
        long start = System.nanoTime();
        log.info("Start loading {} files", files.length);
        for (File file : files) {
            log.info("Loading {} ...", file);
            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    if (!line.trim().isEmpty()) {
                        Splitter.onPattern("[ \\,\\.\\;\\-\\!\\?'\"]").trimResults().split(line).forEach(word -> {
                            if (!word.isEmpty()) {
                                word = word.toLowerCase();
                                final Long count = wordCounterMap.putIfAbsent(word, 1L);
                                if (count != null) {
                                    wordCounterMap.put(word, count + 1L);
                                } else {
                                    accessCounterMapBuilder.put(word, new LongAdder());
                                }
                            }
                        });
                    }
                }
            }
        }
        log.info("Loaded files in {} seconds", TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - start));
        return new CounterMaps(ImmutableMap.<String, Long>builder().putAll(wordCounterMap).build(), accessCounterMapBuilder);
    }



    public static class CounterMaps {
        final ImmutableMap<String, Long> wordCounterMap;
        final ConcurrentHashMap<String, LongAdder> wordAccessMap;

        public CounterMaps(final ImmutableMap<String, Long> wordCounterMap, final ConcurrentHashMap<String, LongAdder> wordAccessMap) {
            this.wordCounterMap = wordCounterMap;
            this.wordAccessMap = wordAccessMap;
        }

        public ImmutableMap<String, Long> getWordCounterMap() {
            return wordCounterMap;
        }

        public ConcurrentHashMap<String, LongAdder> getWordAccessMap() {
            return wordAccessMap;
        }
    }
}
