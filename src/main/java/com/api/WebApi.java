package com.api;

import com.domain.WordInfo;
import com.google.common.collect.ImmutableMap;
import com.internal.FileLoader;
import com.internal.WordsManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;

/**
 * Created by sshumilov on 17.06.16.
 */
@Path("/wordsApi")
@Produces(MediaType.APPLICATION_JSON)
public class WebApi {

    private static final Logger log = LoggerFactory.getLogger(WebApi.class);

    private final WordsManager wordsManager;

    public WebApi(WordsManager manager) throws IOException {
        this.wordsManager = manager;
    }

    @GET
    @Path("{word}")
    public WordInfo findWord(@PathParam("word") String word) {
        log.debug("Call for word {}", word);
        return wordsManager.findWordInfo(word.toLowerCase());
    }
}
