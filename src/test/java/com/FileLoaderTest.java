package com;

import com.domain.WordInfo;
import com.internal.FileLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by sshumilov on 17.06.16.
 */
public class FileLoaderTest {

    private FileLoader loader;

    @Before
    public void setUp() throws Exception {
        final URL resource = getClass().getResource("/test.txt");
        File testTxt = new File(resource.getFile());
        loader = new FileLoader();
        loader.load(new File[]{testTxt});
    }

    @Test
    public void loadedWordsNumTest() throws Exception {
        final URL resource = getClass().getResource("/test.txt");
        File testTxt = new File(resource.getFile());
        FileLoader loader = new FileLoader();
        final FileLoader.CounterMaps maps = loader.load(new File[]{testTxt});
        final int size = maps.getWordCounterMap().keySet().size();
        Assert.assertEquals(size, 1);
        final Long count = maps.getWordCounterMap().get("test");
        Assert.assertEquals(4L, count.longValue());
    }


}
