package com;

import com.domain.WordInfo;
import com.internal.FileLoader;
import com.internal.WordsManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Created by sshumilov on 17.06.16.
 */
public class WordsManagerTest {

    WordsManager manager;
    @Before
    public void setUp() throws Exception {
        final URL resource = getClass().getResource("/test.txt");
        File testTxt = new File(resource.getFile());
        FileLoader loader = new FileLoader();
        final FileLoader.CounterMaps maps = loader.load(new File[]{testTxt});
        manager = new WordsManager(maps.getWordCounterMap(), maps.getWordAccessMap());

    }

    @org.junit.Test
    public void concurrentCallNumTest() throws Exception {
        int count = 100000;
        String[] words = new String[]{"test", "project", "decide"};
        for (String word : words) {
            spamWord(word, count);
        }
        for (String word : words) {
            final WordInfo toTest = manager.findWordInfo(word);
            Assert.assertEquals(toTest.getCallCount(), count + 1);
            Assert.assertEquals(toTest.getWord(), word);
        }
    }


    private void spamWord(String word, int count) throws InterruptedException, ExecutionException {
        final ExecutorService service = Executors.newFixedThreadPool(20);
        List<Callable<WordInfo>> tasks = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            tasks.add(() ->  {
                final WordInfo wordInfo = manager.findWordInfo(word);
                Assert.assertEquals(wordInfo.getWord(), word);
                return wordInfo;
            });
        }
        final List<Future<WordInfo>> futures = service.invokeAll(tasks, 10, TimeUnit.SECONDS);
        for (Future<WordInfo> future : futures) {
            future.get();
        }
        service.shutdownNow();
    }
}
